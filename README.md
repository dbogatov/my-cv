# Dmytro's CV

The up-to-date version of the paper is built in CI and resides as artifact.

> To view the latest PDF, click on a badge `PDF | view online` at the top of the project page in GitLab.

## How to compile

```bash
bash ./document/build.sh # to compile
open ./document/dist/*.pdf # to open
```

If using with Overleaf, it is suggested to run `git config core.fileMode false`.
